
 <link href="/control-de-citas-medicas/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="/control-de-citas-medicas/css/sb-admin-2.min.css" rel="stylesheet">


<!-- Begin Page Content -->
<div class="container-fluid">
<?php
include_once 'Citas.class.php';
$citas= new Citas($DB_con);
if(isset($_POST['btn-save']))
{	
 $codDoctor = $_POST['codDoctor'];
 $codPaciente = $_POST['codPaciente'];
 $fecha = $_POST['fecha'];
 $hora = $_POST['hora'];
 $gravedad = $_POST['gravedad'];
 
 if($citas->create($codDoctor,$codPaciente,$fecha,$hora,$gravedad))
 {
  header("location:Listar.php?insertado");
 }
 else
 {
  header("location:Listar.php?Error");
 }
}
?>
<div class="clearfix"></div>

<?php
if(isset($_GET['insertado']))
{
 ?>
    <div class="container">
 <div class="alert alert-info">
    <strong></strong>Insertado Correctamente<a href="Listar.php">Listar</a>!
 </div>
 </div>
    <?php
}
else if(isset($_GET['Error']))
{
 ?>
    <div class="container">
 <div class="alert alert-warning">
    <strong>Lo sentimos!</strong> Error al insertar !
 </div>
 </div>
    <?php
}
?>

<div class="clearfix"></div><br />

<div class="container">

  
  <form method='post'>
    <table class='table table-bordered'>
 
        <tr>
            <td>Doctor</td>
            <td><input type='text' name='codDoctor' class='form-control' required></td>
        </tr>
 
        <tr>
            <td>Paciente</td>
            <td><input type='text' name='codPaciente' class='form-control' required></td>
        </tr>
 
        <tr>
            <td>Fecha</td>
            <td><input type='date' name='fecha' class='form-control' required></td>
        </tr>

        <tr>
            <td>Hora</td>
            <td><input type='text' name='hora' class='form-control' required></td>
        </tr>
        <tr>
            <td>Gravedad</td>
            <td><input type='text' name='gravedad' class='form-control' required></td>
        </tr>

        <tr>
            <td colspan="2">
            <button type="submit" class="btn btn-primary" name="btn-save">
      <span class="glyphicon glyphicon-plus"></span> Guardar
   </button>  
            <a href="Listar.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Regresar</a>
            </td>
        </tr>
      </table>
</form>
     
     
</div>

</div>
