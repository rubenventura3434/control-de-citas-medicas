<!-- Topbar -->
<?php
include_once "Citas.class.php";
$citas= new Citas($DB_con);
if(isset($_POST['btn-update']))
{
 $id = $_GET['edit_id'];
 $codDoctor = $_POST['codDoctor'];
 $codPaciente = $_POST['codPaciente'];
 $fecha = $_POST['fecha'];
 $hora = $_POST['hora'];
 $gravedad = $_POST['gravedad'];

 
 if($citas->update($id,$codDoctor,$codPaciente,$fecha,$hora,$gravedad))
 {
  $msg = "<div class='alert alert-info'>
    <strong>Listo!</strong>Registro actualizado correctamente<a href='Listar.php'>HOME</a>!
    </div>";
 }
 else
 {
  $msg = "<div class='alert alert-warning'>
    <strong>SORRY!</strong>Error al actualizar!
    </div>";
 }
}

if(isset($_GET['edit_id']))
{
 $id = $_GET['edit_id'];
 extract($citas->getID($id)); 
}

?>

<div class="clearfix"></div>

<div class="container">
<?php
if(isset($msg))
{
 echo $msg;
}
?>
</div>

<link href="/control-de-citas-medicas/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="/control-de-citas-medicas/css/sb-admin-2.min.css" rel="stylesheet">
<div class="clearfix"></div><br />

<div class="container">
  
     <form method='post'>
 
    <table class='table table-bordered'>
 
        <tr>
            <td>Doctor</td>
            <td><input type='text' name='codDoctor' class='form-control' value="<?php echo $codDoctor; ?>" required></td>
        </tr>
 
        <tr>
            <td>Paciente</td>
            <td><input type='text' name='codPaciente' class='form-control' value="<?php echo $codPaciente; ?>" required></td>
        </tr>
 
        <tr>
            <td>Fecha</td>
            <td><input type='date' name='fecha' class='form-control' value="<?php echo $fecha; ?>" required></td>
        </tr>

         <tr>
            <td>Hora</td>
            <td><input type='text' name='hora' class='form-control' value="<?php echo $hora; ?>" required></td>
        </tr>

         <tr>
            <td>Gravedad</td>
            <td><input type='text' name='gravedad' class='form-control' value="<?php echo $gravedad; ?>" required></td>
        </tr>

        <tr>
            <td colspan="2">
                <button type="submit" class="btn btn-primary" name="btn-update">
       <span class="glyphicon glyphicon-edit"></span>  Actualizar este registro
    </button>
                <a href="Listar.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Cancelar</a>
            </td>
        </tr>
 
    </table>
</form>
     
     
</div>
