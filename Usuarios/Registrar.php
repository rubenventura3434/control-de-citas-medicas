
 <link href="/control-de-citas-medicas/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="/control-de-citas-medicas/css/sb-admin-2.min.css" rel="stylesheet">


<!-- Begin Page Content -->
<div class="container-fluid">
<?php
include_once 'Usuario.class.php';
$usuarios= new Usuarios($DB_con);
if(isset($_POST['btn-save']))
{ 
 $usuario = $_POST['usuario'];
 $apellido = $_POST['apellido'];
 $pass = base64_encode(($_POST['pass']));
 
 if($usuarios->create($usuario,$apellido,$pass))
 {
  header("location:Listar.php?insertado");
 }
 else
 {
  header("location:Listar.php?Error");
 }
}
?>
<div class="clearfix"></div>

<?php
if(isset($_GET['insertado']))
{
 ?>
    <div class="container">
 <div class="alert alert-info">
    <strong></strong>Insertado Correctamente<a href="Listar.php">Listar</a>!
 </div>
 </div>
    <?php
}
else if(isset($_GET['Error']))
{
 ?>
    <div class="container">
 <div class="alert alert-warning">
    <strong>Lo sentimos!</strong> Error al insertar !
 </div>
 </div>
    <?php
}
?>

<div class="clearfix"></div><br />

<div class="container">

  
  <form method='post'>
 
        <tr>
            <td>Usuario</td>
            <td><input type='text' name='usuario' class='form-control' required></td>
        </tr>
 
        <tr>
            <td>Apellido</td>
            <td><input type='text' name='apellido' class='form-control' required></td>
        </tr>
 
        <tr>
            <td>Pass</td>
            <td><input type='password' name='pass' class='form-control' required></td>
        </tr>
        <tr>
            <td colspan="2">
            <button type="submit" class="btn btn-primary" name="btn-save">
      <span class="glyphicon glyphicon-plus"></span> Guardar
   </button>  
            <a href="Listar.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Regresar</a>
            </td>
        </tr>
</form>
     
     
</div>

</div>
