<?php
include_once "Usuario.class.php";
$usuarios= new Usuarios($DB_con);
if(isset($_POST['btn-update']))
{
 $id = $_GET['edit_id'];
 $usuario = $_POST['usuario'];
 $pass = base64_encode($_POST['pass']);

 
 if($usuarios->update($id,$usuario,$pass))
 {
  $msg = "<div class='alert alert-info'>
    <strong>Listo!</strong>Registro actualizado correctamente<a href='Listar.php'>HOME</a>!
    </div>";
 }
 else
 {
  $msg = "<div class='alert alert-warning'>
    <strong>Lo siento! </strong>Error al actualizar!
    </div>";
 }
}

if(isset($_GET['edit_id']))
{
 $id = $_GET['edit_id'];
 extract($usuarios->getID($id)); 
}

?>

<div class="clearfix"></div>

<div class="container">
<?php
if(isset($msg))
{
 echo $msg;
}
?>
</div>

<link href="/control-de-citas-medicas/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="/control-de-citas-medicas/css/sb-admin-2.min.css" rel="stylesheet">
<div class="clearfix"></div><br />

<div class="container">
  
     <form method='post'>
 
    <table class='table table-bordered'>
 
        <tr>
            <td>Usuario</td>
            <td><input type='text' name='usuario' class='form-control' value="<?php echo $usuario; ?>" required></td>
        </tr>
        <tr>
            <td>Pass</td>
            <td><input type='text' name='pass' class='form-control' value="<?php echo base64_decode($pass); ?>" required></td>
        </tr>

        <tr>
            <td colspan="2">
                <button type="submit" class="btn btn-primary" name="btn-update">
       <span class="glyphicon glyphicon-edit"></span>  Actualizar este registro
    </button>
                <a href="Listar.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Cancelar</a>
            </td>
        </tr>
 
    </table>
</form>
     
     
</div>
