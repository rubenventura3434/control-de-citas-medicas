<?php
include_once 'Empleados.class.php';
$empleado= new Empleados($DB_con);
if(isset($_POST['btn-del']))
{
 $id = $_GET['delete_id'];
 $empleado->delete($id);
 header("Location: Listar.php?deleted"); 
}
?>
<link href="/control-de-citas-medicas/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="/control-de-citas-medicas/css/sb-admin-2.min.css" rel="stylesheet">
<div class="clearfix"></div>

<div class="container">

 <?php
 if(isset($_GET['deleted']))
 {
  ?>
        <div class="alert alert-success">
     <strong>Exito!</strong> Datos Eliminados.....
  </div>
        <?php
 }
 else
 {
  ?>
        <div class="alert alert-danger">
     <strong>Seguro!</strong> Deseas eliminar el siguiente registro? 
  </div>
        <?php
 }
 ?> 
</div>

<div class="clearfix"></div>

<div class="container">
  
  <?php
  if(isset($_GET['delete_id']))
  {
   ?>
         <table class='table table-bordered'>
         <tr>
                            <td>#</td>
                            <td>Codigo</td>
                            <td>Nombre</td>
                            <td>Apellido</td>
                            <td>Especialidad</td>
                            <td>Telefono</td>
                            <td>Direccion</td>
                            <td>Nacionalidad</td>
                            <td>DUI</td>
                            <td>Genero</td>
                            <td>Id_usuario</td>
         </tr>
         <?php
         $stmt = $DB_con->prepare("SELECT * FROM doctor WHERE id=:id");
         $stmt->execute(array(":id"=>$_GET['delete_id']));
         while($row=$stmt->fetch(PDO::FETCH_BOTH))
         {
             ?>
             <tr>
                <td><?php print($row['id']); ?></td>
                <td><?php print($row['codigo']); ?></td>
                <td><?php print($row['nombre']); ?></td>
                <td><?php print($row['apellido']); ?></td>
                <td><?php print($row['especialidad']); ?></td>
                <td><?php print($row['telefono']); ?></td>
                <td><?php print($row['direccion']); ?></td>
                <td><?php print($row['nacionalidad']); ?></td>
                <td><?php print($row['dui']); ?></td>
                <td><?php print($row['genero']); ?></td>
                <td><?php print($row['id_usuario']); ?></td>
             </tr>
             <?php
         }
         ?>
         </table>
         <?php
  }
  ?>
</div>

<div class="container">
<p>
<?php
if(isset($_GET['delete_id']))
{
 ?>
   <form method="post">
    <input type="hidden" name="id" value="<?php echo $row['id']; ?>" />
    <button class="btn btn-large btn-primary" type="submit" name="btn-del"><i class="glyphicon glyphicon-trash"></i> &nbsp; SI</button>
    <a href="Listar.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; NO</a>
    </form>  
 <?php
}
else
{
 ?>
    <a href="Listar.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Regresar</a>
    <?php
}
?>
</p>
</div>