
 <link href="/control-de-citas-medicas/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="/control-de-citas-medicas/css/sb-admin-2.min.css" rel="stylesheet">


<!-- Begin Page Content -->
<div class="container-fluid">
<?php
include_once 'Empleados.class.php';
$empleado= new Empleados($DB_con);
if(isset($_POST['btn-save']))
{	
 $codigo = $_POST['codigo'];
 $nombre = $_POST['nombre'];
 $apellido = $_POST['apellido'];
 $especialidad = $_POST['especialidad'];
 $telefono = $_POST['telefono'];
 $direccion = $_POST['direccion'];
 $nacionalidad = $_POST['nacionalidad'];
 $dui = $_POST['dui'];
 $genero = $_POST['genero'];
 $id_usuario = $_POST['id_usuario'];
 
 if($empleado->create($codigo,$nombre,$apellido,$especialidad,$telefono,$direccion,$nacionalidad,$dui,$genero,$id_usuario))
 {
  header("location:Listar.php?insertado");
 }
 else
 {
  header("location:Listar.php?Error");
 }
}
?>
<div class="clearfix"></div>

<?php
if(isset($_GET['insertado']))
{
 ?>
    <div class="container">
 <div class="alert alert-info">
    <strong></strong>Insertado Correctamente<a href="Listar.php">Listar</a>!
 </div>
 </div>
    <?php
}
else if(isset($_GET['Error']))
{
 ?>
    <div class="container">
 <div class="alert alert-warning">
    <strong>Lo sentimos!</strong> Error al insertar !
 </div>
 </div>
    <?php
}
?>

<div class="clearfix"></div><br />

<div class="container">

  
  <form method='post'>
    <table class='table table-bordered'>
 
        <tr>
            <td>Codigo:</td>
            <td><input type='text' name='codigo' class='form-control' required></td>
        </tr>
 
        <tr>
            <td>Nombre:</td>
            <td><input type='text' name='nombre' class='form-control' required></td>
        </tr>
 
        <tr>
            <td>Apellido:</td>
            <td><input type='text' name='apellido' class='form-control' required></td>
        </tr>

        <tr>
            <td>Especialidad:</td>
            <td><input type='text' name='especialidad' class='form-control' required></td>
        </tr>
        <tr>
            <td>Telefono:</td>
            <td><input type='text' name='telefono' class='form-control' required></td>
        </tr>

        <tr>
            <td>Direccion:</td>
            <td><input type='text' name='direccion' class='form-control' required></td>
        </tr>

        <tr>
            <td>Nacionalidad:</td>
            <td><input type='text' name='nacionalidad' class='form-control' required></td>
        </tr>

        <tr>
            <td>DUI:</td>
            <td><input type='text' name='dui' class='form-control' required></td>
        </tr>

        <tr>
            <td>Genero:</td>
            <td><input type='text' name='genero' class='form-control' required></td>
        </tr>

        <tr>
            <td>id_usuario:</td>
            <td><input type='text' name='id_usuario' class='form-control' required></td>
        </tr>

        <tr>
            <td colspan="2">
            <button type="submit" class="btn btn-primary" name="btn-save">
      <span class="glyphicon glyphicon-plus"></span> Guardar
   </button>  
            <a href="Listar.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Regresar</a>
            </td>
        </tr>
      </table>
</form>
     
     
</div>

</div>
