<!-- Topbar -->
<?php
include_once "Empleados.class.php";
$empleado= new Empleados($DB_con);
if(isset($_POST['btn-update']))
{
 $id = $_GET['edit_id'];
 $codigo = $_POST['codigo'];
 $nombre = $_POST['nombre'];
 $apellido = $_POST['apellido'];
 $especialidad = $_POST['especialidad'];
 $telefono = $_POST['telefono'];
$direccion = $_POST['direccion'];
$nacionalidad = $_POST['nacionalidad'];
$dui = $_POST['dui'];
$genero = $_POST['genero'];
$id_usuario = $_POST['id_usuario'];
 
 if($empleado->update($id,$codigo,$nombre,$apellido,$especialidad,$telefono,$direccion,$nacionalidad,$dui,$genero,$id_usuario))
 {
  $msg = "<div class='alert alert-info'>
    <strong>Listo!</strong>Registro actualizado correctamente<a href='Listar.php'>HOME</a>!
    </div>";
 }
 else
 {
  $msg = "<div class='alert alert-warning'>
    <strong>Lo sentimos!</strong>Error al actualizar!
    </div>";
 }
}

if(isset($_GET['edit_id']))
{
 $id = $_GET['edit_id'];
 extract($empleado->getID($id)); 
}

?>

<div class="clearfix"></div>

<div class="container">
<?php
if(isset($msg))
{
 echo $msg;
}
?>
</div>

<link href="/control-de-citas-medicas/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="/control-de-citas-medicas/css/sb-admin-2.min.css" rel="stylesheet">
<div class="clearfix"></div><br />

<div class="container">
  
     <form method='post'>
 
    <table class='table table-bordered'>
 
        <tr>
            <td>Codigo:</td>
            <td><input type='text' name='codigo' class='form-control' value="<?php echo $codigo; ?>" required></td>
        </tr>
 
        <tr>
            <td>Nombre:</td>
            <td><input type='text' name='nombre' class='form-control' value="<?php echo $nombre; ?>" required></td>
        </tr>
 
        <tr>
            <td>Apellido:</td>
            <td><input type='text' name='apellido' class='form-control' value="<?php echo $apellido; ?>" required></td>
        </tr>

         <tr>
            <td>Especialidad:</td>
            <td><input type='text' name='especialidad' class='form-control' value="<?php echo $especialidad; ?>" required></td>
        </tr>

         <tr>
            <td>Telefono:</td>
            <td><input type='text' name='telefono' class='form-control' value="<?php echo $telefono; ?>" required></td>
        </tr>

         <tr>
            <td>Direccion:</td>
            <td><input type='text' name='direccion' class='form-control' value="<?php echo $direccion; ?>" required></td>
        </tr>

         <tr>
            <td>Nacionalidad:</td>
            <td><input type='text' name='nacionalidad' class='form-control' value="<?php echo $nacionalidad; ?>" required></td>
        </tr>

         <tr>
            <td>DUI:</td>
            <td><input type='text' name='dui' class='form-control' value="<?php echo $dui; ?>" required></td>
        </tr>

        <tr>

        <tr>
            <td>Genero:</td>
            <td><input type='text' name='genero' class='form-control' value="<?php echo $genero; ?>" required></td>
        </tr>

         <tr>
            <td>Id_usuario:</td>
            <td><input type='text' name='id_usuario' class='form-control' value="<?php echo $id_usuario; ?>" required></td>
        </tr>
            <td colspan="2">
                <button type="submit" class="btn btn-primary" name="btn-update">
       <span class="glyphicon glyphicon-edit"></span>  Actualizar este registro
    </button>
                <a href="Listar.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Cancelar</a>
            </td>
        </tr>
 
    </table>
</form>
     
     
</div>
