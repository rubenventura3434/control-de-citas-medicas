<!-- Topbar -->
<?php 
include 'navbar.php';
$consultacitas = $DB_con->prepare("SELECT count(*) FROM citas");
$consultamedicos = $DB_con->prepare("SELECT count(*) FROM doctor");
$consultapaciente = $DB_con->prepare("SELECT count(*) FROM pacientes");

$consultacitas->execute();
$consultamedicos->execute();
$consultapaciente->execute();

$citas = $consultacitas->fetch()[0];
$medicos = $consultamedicos->fetch()[0];
$pacientes = $consultapaciente->fetch()[0];

 ?>
<!-- End of Topbar -->

<!-- Begin Page Content -->
<div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generar Reporte</a>
          </div>

          <!-- Content Row -->
          <div class="row">

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Citas Pendientes</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $citas; ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Medicos Disponibles</div>
                      <div class="row no-gutters align-items-center">
                        <div class="col-auto">
                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?php echo $medicos; ?></div>
                        </div>
                        
                      </div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Total de Pacientes</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $pacientes; ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-procedures fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Content Row -->

</div>

<?php include 'footer.php'; ?>
