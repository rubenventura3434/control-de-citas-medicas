<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-stethoscope"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Control de Citas</div>
      </a>
      
   

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="/control-de-citas-medicas/home.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      

      <!-- Nav Item - Pages Collapse Menu -->
      <?php
      if ($doctor) { ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseDoctor" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-user-md"></i>
          <span>Doctor</span>
        </a>
        <div id="collapseDoctor" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Doctor:</h6>
            <a class="collapse-item" href="/control-de-citas-medicas/Doctor/Citas.php">Mis Citas</a>
        </div>
      </li>
      <?php
    } else { ?>
      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePacientes" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-procedures"></i>
          <span>Pacientes</span>
        </a>
        <div id="collapsePacientes" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Pacientes:</h6>
            <a class="collapse-item" href="/control-de-citas-medicas/Pacientes/Listar.php">Listar</a> 
            <a class="collapse-item" href="/control-de-citas-medicas/Pacientes/Registrar.php">Registrar</a> 
        </div>
      </li>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCitas" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-notes-medical"></i>
          <span>Citas</span>
        </a>
        <div id="collapseCitas" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Citas:</h6>
            <a class="collapse-item" href="/control-de-citas-medicas/Citas/Listar.php">Listar</a>
        </div>
      </li>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseHistorial" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-history"></i>
          <span>Historial</span>
        </a>
        <div id="collapseHistorial" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Historial:</h6>
            <a class="collapse-item" href="/control-de-citas-medicas/Historial/Listar.php">Ver Historial</a> 
        </div>
      </li>
      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUsuarios" aria-expanded="true" aria-controls="collapseUsuarios">
          <i class="fas fa-user"></i>
          <span>Usuarios</span>
        </a>
        <div id="collapseUsuarios" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Usuarios:</h6>
            <a class="collapse-item" href="/control-de-citas-medicas/Usuarios/Listar.php">Listar</a>
            <a class="collapse-item" href="/control-de-citas-medicas/Usuarios/Registrar.php">Registrar</a> 
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseEmpleados" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-folder-open"></i>
          <span>Empleados</span>
        </a>
        <div id="collapseEmpleados" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Empleados:</h6>
            <a class="collapse-item" href="/control-de-citas-medicas/Empleado/Listar.php">Listar</a> 
        </div>
      </li>
<?php } ?>
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>