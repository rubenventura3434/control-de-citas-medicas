<?php

require_once "../Database/Conexion.class.php";

class Paciente
{
 private $db;
 
 function __construct($DB_con)
 {
  $this->db = $DB_con;
 }
 
 public function create($expediente, $nombres, $apellidos, $telefono, $direccion, $nombrem,$nombrep, $genero, $fechanaci,$padecimiento, $nacionalidad)
 {
  try
  {
   $stmt = $this->db->prepare("INSERT INTO pacientes(expediente, nombres, apellidos, telefono,direccion, nombre_madre,nombre_padre, genero,fecha_nac,padecimiento, nacionalidad) VALUES(:expediente, :nombres, :apellidos, :telefono, :direccion, :nombrem,:nombrep,  :genero, :fechanaci,:padecimiento, :nacionalidad)");
   $stmt->bindparam(":expediente",$expediente);
   $stmt->bindparam(":nombres",$nombres);
   $stmt->bindparam(":apellidos",$apellidos);
   $stmt->bindparam(":telefono",$telefono);
   $stmt->bindparam(":direccion",$direccion);
   $stmt->bindparam(":nombrem",$nombrem);
   $stmt->bindparam(":nombrep",$nombrep);
   $stmt->bindparam(":genero",$genero);
   $stmt->bindparam(":fechanaci",$fechanaci);
   $stmt->bindparam(":padecimiento",$padecimiento);
   $stmt->bindparam(":nacionalidad",$nacionalidad);
   $stmt->execute();
   return true;
  }
  catch(PDOException $e)
  {
   echo $e->getMessage(); 
   return false;
  }
  
 }
 
 public function getID($id)
 {
  $stmt = $this->db->prepare("SELECT * FROM pacientes WHERE id=:id");
  $stmt->execute(array(":id"=>$id));
  $editRow=$stmt->fetch(PDO::FETCH_ASSOC);
  return $editRow;
 }
 
 public function update($id,$expediente, $nombres, $apellidos, $telefono, $direccion, $nombrem,$nombrep, $genero, $fechanaci,$padecimiento, $nacionalidad)
 {
  try
  {
   $stmt=$this->db->prepare("UPDATE pacientes SET
                expediente=:expediente,
                nombres=:nombres,
                apellidos=:apellidos,
                telefono=:telefono,
                direccion=:direccion,
                nombre_madre=:nombrem,
                nombre_padre=:nombrep,
                genero=:genero,
                fecha_nac=:fechanaci,
                padecimiento=:padecimiento,
                nacionalidad=:nacionalidad
             WHERE id=:id ");
   $stmt->bindparam(":expediente",$expediente);
   $stmt->bindparam(":nombres",$nombres);
   $stmt->bindparam(":apellidos",$apellidos);
   $stmt->bindparam(":telefono",$telefono);
   $stmt->bindparam(":direccion",$direccion);
   $stmt->bindparam(":nombrem",$nombrem);
   $stmt->bindparam(":nombrep",$nombrep);
   $stmt->bindparam(":genero",$genero);
   $stmt->bindparam(":fechanaci",$fechanaci);
   $stmt->bindparam(":padecimiento",$padecimiento);
   $stmt->bindparam(":nacionalidad",$nacionalidad);
   $stmt->bindparam(":id",$id);
   $stmt->execute();
header("location:Listar.php");

   return true; 
  }
  catch(PDOException $e)
  {
   echo $e->getMessage(); 
   return false;
  }
 }
 
 public function delete($id)
 {
  $stmt = $this->db->prepare("DELETE FROM pacientes WHERE id=:id");
  $stmt->bindparam(":id",$id);
  $stmt->execute();
  return true;
 }
 
 /* paging */
 
 public function dataview($query)
 {
  $stmt = $this->db->prepare($query);
  $stmt->execute();
 
  if($stmt->rowCount()>0)
  {
   while($row=$stmt->fetch(PDO::FETCH_ASSOC))
   {
    ?>
                <tr>
                <td><?php echo($row['id']); ?></td>
                <td><?php echo($row['expediente']); ?></td>
                <td><?php echo($row['nombres']); ?></td>
                <td><?php echo($row['apellidos']); ?></td>
                <td><?php echo($row['telefono']); ?></td>
                <td><?php echo($row['direccion']); ?></td>
                <td><?php echo($row['nombre_madre']); ?></td>
                <td><?php echo($row['nombre_padre']); ?></td>
                <td><?php echo($row['genero']); ?></td>
                <td><?php echo($row['fecha_nac']); ?></td>
                 <td><?php echo($row['padecimiento']); ?></td>
                <td><?php echo($row['nacionalidad']); ?></td>
                <td align="center">
                <a href="Editar.php?edit_id=<?php print($row['id']); ?>"><i class="fas fa-marker"></i></a>
                </td>
                <td align="center">
                <a href="delete.php?delete_id=<?php print($row['id']); ?>"><i class="fas fa-trash-alt"></i></a>
                </td>
                </tr>
                <?php
   }
  }
  else
  {
   ?>
            <tr>
            <td>Sin Datos......</td>
            </tr>
            <?php
  }
  
 }
 public function paging($query,$records_per_page)
 {
  $starting_position=0;
  if(isset($_GET["page_no"]))
  {
   $starting_position=($_GET["page_no"]-1)*$records_per_page;
  }
  $query2=$query." limit $starting_position,$records_per_page";
  return $query2;
 }
 
 public function paginglink($query,$records_per_page)
 {
  
  $self = $_SERVER['PHP_SELF'];
  
  $stmt = $this->db->prepare($query);
  $stmt->execute();
  
  $total_no_of_records = $stmt->rowCount();
  
  if($total_no_of_records > 0)
  {
   ?><ul class="pagination"><?php
   $total_no_of_pages=ceil($total_no_of_records/$records_per_page);
   $current_page=1;
   if(isset($_GET["page_no"]))
   {
    $current_page=$_GET["page_no"];
   }
   if($current_page!=1)
   {
    $previous =$current_page-1;
    echo "<li><a href='".$self."?page_no=1'>Principio</a></li>";
    echo "<li><a href='".$self."?page_no=".$previous."'>Anterior</a></li>";
   }
   for($i=1;$i<=$total_no_of_pages;$i++)
   {
    if($i==$current_page)
    {
     echo "<li><a href='".$self."?page_no=".$i."' style='color:red;'>".$i."</a></li>";
    }
    else
    {
     echo "<li><a href='".$self."?page_no=".$i."'>".$i."</a></li>";
    }
   }
   if($current_page!=$total_no_of_pages)
   {
    $next=$current_page+1;
    echo "<li><a href='".$self."?page_no=".$next."'>Siguiente</a></li>";
    echo "<li><a href='".$self."?page_no=".$total_no_of_pages."'>Anterior</a></li>";
   }
   ?></ul><?php
  }
 }
 
public function show($result){
$stmt = $this->db->prepare($result);
$stmt->execute();
if ($stmt->rowCount()> 0) {
  while ($row-$stmt->fetch(PDO::FETCH_ASSOC)) {

    extract($row);
    echo $id;
    echo $expediente;
    echo $nombres;
    echo $apellidos;
    echo $telefono;
    echo $direccion;
    echo $nombrem;
    echo $nombrep;
    echo $genero;
    echo $fechanaci;
    echo $padecimiento;
    echo $nacionalidad;
    echo "</br>";
  }
  }
else{
    echo "no hay datos";
}
}
 
}

