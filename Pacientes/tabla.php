  <!-- Custom styles for this template-->
  <link href="/control-de-citas-medicas/css/sb-admin-2.min.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<!-- Begin Page Content -->
<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<!-- JS --> 
<script type="text/javascript" src="js/jquery-3.3.1.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<!--Fin-->

<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/tabla.css">

<!-- CSS -->
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap.min.css">
<!--Fin--> 

<!-- Codigo tabla -->
<script type="text/javascript" src="js/datatable.js"></script>
<!--Fin--> 
<div class="container-fluid">
  <body>
  <div class="clearfix"></div>

<div class="container">
<a href="Registrar.php" class="btn btn-large btn-info"><i class="glyphicon glyphicon-plus"></i> &nbsp; Nuevo</a>
</div>

<div class="container">
  
</div>
<div class="clearfix"></div><br />
          <center>
 <div class="container" style="margin-top: 20px">
        <div class="row">
            <div class="col-md-8">
                <table class="table table-bordered table-striped table-hover" style="width:100%" id="myTable">
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>Expediente</td>
                            <td>Nombres</td>
                            <td>Apellidos</td>
                            <td>Telefono</td>
                            <td>Direccion</td>
                            <td>Nombre Madre</td>
                            <td>Nombre Padre</td>
                            <td>Genero</td>
                            <td>Fecha de Nacimiento</td>
                            <td>Padecimiento</td>
                            <td>Nacionalidad</td>
                            <td colspan="2">Acciones</td>
                        </tr>
                    </thead>
                    <tbody>
     <?php
     require_once "Paciente.class.php";
  $query = "SELECT * FROM pacientes";
  $records_per_page=10;   
  $paciente = new Paciente($DB_con);  
  $newquery = $paciente->paging($query,$records_per_page);  
  $paciente->dataview($newquery);
  ?>
</tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="pagination-wrap" align="center">
            <?php $paciente->paginglink($query,$records_per_page); ?>
         </div>
  </center>
</body>