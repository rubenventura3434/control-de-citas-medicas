 <link href="/control-de-citas-medicas/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="/control-de-citas-medicas/css/sb-admin-2.min.css" rel="stylesheet">


<!-- Begin Page Content -->
<div class="container-fluid">
<?php
include_once 'Paciente.class.php';
 $paciente = new Paciente($DB_con); 
if(isset($_POST['btn-save']))
{	
    $expediente=$_POST['expediente'];  
    $nombres=$_POST['nombres'];
    $apellidos=$_POST['apellidos'];
    $telefono=$_POST['telefono'];
    $direccion=$_POST['direccion'];
    $nombrem=$_POST['nombrem'];
    $nombrep=$_POST['nombrep'];
    $genero=$_POST['genero'];
    $fechanaci=$_POST['fechanaci'];
    $padecimiento=$_POST['padecimiento'];
    $nacionalidad=$_POST['nacionalidad'];
 
 
 if($paciente->create($expediente,$nombres, $apellidos, $telefono, $direccion, $nombrem,$nombrep, $genero, $fechanaci,$padecimiento, $nacionalidad))
 {
  header("location:Listar.php?insertado");
 }
 else
 {
  header("location:Listar.php?Error");
 }
}
?>
<div class="clearfix"></div>

<?php
if(isset($_GET['insertado']))
{
 ?>
    <div class="container">
 <div class="alert alert-info">
    <strong></strong>Insertado Correctamente<a href="Listar.php">Listar</a>!
 </div>
 </div>
    <?php
}
else if(isset($_GET['Error']))
{
 ?>
    <div class="container">
 <div class="alert alert-warning">
    <strong>Lo sentimos!</strong> Error al insertar !
 </div>
 </div>
    <?php
}
?>

<div class="clearfix"></div><br />

<div class="container">

  
  <form method='post'>
 
        <tr>
            <td>Expediente del Paciente</td>
            <td><input type='text' name='expediente' class='form-control' required></td>
        </tr>
        <tr>
            <td>Nombres del Paciente</td>
            <td><input type='text' name='nombres' class='form-control' required></td>
        </tr>
 
        <tr>
            <td>Apellido del Paciente</td>
            <td><input type='text' name='apellidos' class='form-control' required></td>
        </tr>
 
        <tr>
            <td>Telefono</td>
            <td><input type='text' name='telefono' class='form-control' required></td>
        </tr>
        <tr>
            <td>Direccion</td>
            <td><input type='text' name='direccion' class='form-control' required></td>
        </tr>
        <tr>
            <td>Nombre de la Madre</td>
            <td><input type='text' name='nombrem' class='form-control' required></td>
        </tr>
         <tr>
            <td>Nombre del Padre</td>
            <td><input type='text' name='nombrep' class='form-control' required></td>
        </tr>
    <tr>
            <td>Genero</td>
            <td><input type='text' name='genero' class='form-control' required></td>
        </tr>
     <tr>
            <td>Fecha de Nacimiento</td>
            <td><input type='date' name='fechanaci' class='form-control' required></td>
        </tr>
         <tr>
            <td>Padecimiento</td>
            <td><input type='text' name='padecimiento' class='form-control' required></td>
        </tr>
        <tr>
            <td>Nacionalidad</td>
            <td><input type='text' name='nacionalidad' class='form-control' required></td>
        </tr>
        <tr>
            <td colspan="2">
            <button type="submit" class="btn btn-primary" name="btn-save">
      <span class="glyphicon glyphicon-plus"></span> Guardar
   </button>  
            <a href="Listar.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Regresar</a>
            </td>
        </tr>
</form>
     
     
</div>

</div>
