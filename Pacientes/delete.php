<?php

include_once 'Paciente.class.php';
$paciente = new Paciente($DB_con); 
if(isset($_POST['btn-del']))
{
 $id = $_GET['delete_id'];
 $paciente->delete($id);
 header("Location: Listar.php?deleted"); 
}
?>
<link href="/control-de-citas-medicas/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="/control-de-citas-medicas/css/sb-admin-2.min.css" rel="stylesheet">
<div class="clearfix"></div>

<div class="container">

 <?php
 if(isset($_GET['deleted']))
 {
  ?>
        <div class="alert alert-success">
     <strong>Exito!</strong> Datos Eliminados.....
  </div>
        <?php
 }
 else
 {
  ?>
        <div class="alert alert-danger">
     <strong>Seguro!</strong> Deseas eliminar el siguiente registro? 
  </div>
        <?php
 }
 ?> 
</div>

<div class="clearfix"></div>

<div class="container">
  
  <?php
  if(isset($_GET['delete_id']))
  {
   ?>
         <table class='table table-bordered'>
         <tr>
         <th>#</th>
         <th>Expediente</th>
         <th>Nombres</th>
        <th>Apellidos</th>
        <th>Telefono</th>
        <th>Direccion</th>
        <th>Nombre Madre</th>
        <th>Nombre Padre</th>
        <th>Fecha de Nacimiento</th>
        <th>Genero</th>
        <th>Padecimiento</th>
        <th>Nacionalidad</th>
         </tr>
         <?php
         $stmt = $DB_con->prepare("SELECT * FROM pacientes WHERE id=:id");
         $stmt->execute(array(":id"=>$_GET['delete_id']));
         while($row=$stmt->fetch(PDO::FETCH_BOTH))
         {
             ?>
             <tr>
             <td><?php print($row['expediente']); ?></td>
             <td><?php print($row['nombres']); ?></td>
             <td><?php print($row['apellidos']); ?></td>
             <td><?php print($row['telefono']); ?></td>
             <td><?php print($row['direccion']); ?></td>
             <td><?php print($row['nombre_madre']); ?></td>
             <td><?php print($row['nombre_padre']); ?></td>
             <td><?php print($row['genero']); ?></td>
             <td><?php print($row['fecha_nac']); ?></td>
             <td><?php print($row['padecimiento']); ?></td>
             <td><?php print($row['nacionalidad']); ?></td>
             </tr>
             <?php
         }
         ?>
         </table>
         <?php
  }
  ?>
</div>

<div class="container">
<p>
<?php
if(isset($_GET['delete_id']))
{
 ?>
   <form method="post">
    <input type="hidden" name="id" value="<?php echo $row['id']; ?>" />
    <button class="btn btn-large btn-primary" type="submit" name="btn-del"><i class="glyphicon glyphicon-trash"></i> &nbsp; SI</button>
    <a href="Listar.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; NO</a>
    </form>  
 <?php
}
else
{
 ?>
    <a href="Listar.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Regresar</a>
    <?php
}
?>
</p>
</div>