<!-- Topbar -->
<?php

include_once 'Paciente.class.php';
$paciente = new Paciente($DB_con); 
if(isset($_POST['btn-update']))
{
 $id = $_GET['edit_id'];
 $expediente=$_POST['expediente'];
    $nombres=$_POST['nombres'];
    $apellidos=$_POST['apellidos'];
    $telefono=$_POST['telefono'];
    $direccion=$_POST['direccion'];
    $nombrem=$_POST['nombrem'];
    $nombrep=$_POST['nombrep'];
    $genero=$_POST['genero'];
    $fechanaci=$_POST['fechanaci'];
    $padecimiento=$_POST['padecimiento'];
    $nacionalidad=$_POST['nacionalidad'];
 

 
 if($paciente->update($id, $expediente,$nombres, $apellidos, $telefono, $direccion, $nombrem,$nombrep, $genero,$fechanaci, $padecimiento,$nacionalidad))
 {
  $msg = "<div class='alert alert-info'>
    <strong>Listo!</strong>Registro actualizado correctamente<a href='Listar.php'>HOME</a>!
    </div>";
 }
 else
 {
  $msg = "<div class='alert alert-warning'>
    <strong>SORRY!</strong>Error al actualizar!
    </div>";
 }
}

if(isset($_GET['edit_id']))
{
 $id = $_GET['edit_id'];
 extract($paciente->getID($id)); 
}

?>

<div class="clearfix"></div>

<div class="container">
<?php
if(isset($msg))
{
 echo $msg;
}
?>
</div>

<link href="/control-de-citas-medicas/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="/control-de-citas-medicas/css/sb-admin-2.min.css" rel="stylesheet">
<div class="clearfix"></div><br />

<div class="container">
  
     <form method='post'>
 
    <table class='table table-bordered'>
 
       <tr>
            <td>Expediente del Paciente</td>
            <td><input type='text' name='expediente' class='form-control' value="<?php echo $expediente; ?>" required></td>
        </tr>
        <tr>
            <td>Nombres del Paciente</td>
            <td><input type='text' name='nombres' class='form-control' value="<?php echo $nombres; ?>" required></td>
        </tr>
 
        <tr>
            <td>Apellido del Paciente</td>
            <td><input type='text' name='apellidos' class='form-control' value="<?php echo $apellidos; ?>" required></td>
        </tr>
 
        <tr>
            <td>Telefono</td>
            <td><input type='text' name='telefono' class='form-control' value="<?php echo $telefono; ?>" required></td>
        </tr>
        <tr>
            <td>Direccion</td>
            <td><input type='text' name='direccion' class='form-control' value="<?php echo $direccion; ?>" required></td>
        </tr>
        <tr>
            <td>Nombre de la Madre</td>
            <td><input type='text' name='nombrem' class='form-control' value="<?php echo $nombre_madre; ?>" required></td>
        </tr>
         <tr>
            <td>Nombre del Padre</td>
            <td><input type='text' name='nombrep' class='form-control' value="<?php echo $nombre_padre; ?>" required></td>
        </tr>
         <tr>
            <td>Genero</td>
            <td><input type='text' name='genero' class='form-control' value="<?php echo $genero; ?>" required></td>
        </tr>
        <tr>
            <td>Fecha de Nacimiento</td>
            <td><input type='date' name='fechanaci' class='form-control' value="<?php echo $fecha_nac; ?>" required></td>
        </tr>
        
        <tr>
            <td>Padecimiento</td>
            <td><input type='text' name='padecimiento' class='form-control' value="<?php echo $padecimiento; ?>" required></td>
        </tr>

        <tr>
            <td>Nacionalidad</td>
            <td><input type='text' name='nacionalidad' class='form-control' value="<?php echo $nacionalidad; ?>" required></td>
        </tr>

        <tr>
            <td colspan="2">
                <button type="submit" class="btn btn-primary" name="btn-update">
       <span class="glyphicon glyphicon-edit"></span>  Actualizar este registro
    </button>
                <a href="Listar.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Cancelar</a>
            </td>
        </tr>
 
    </table>
</form>
     
     
</div>
