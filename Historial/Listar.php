<!-- Topbar -->
<?php include '../navbar.php'; ?>
<!-- End of Topbar -->

<!-- Begin Page Content -->
<div class="container-fluid">

<?php
		require '../Database/Conexion.class.php';
		$doctor = $datos["codigo"];
		$filtro = "";
		if ($_GET) {
			if ($_GET["tipo"] == "doctor") {
				$filtro = "WHERE historial.codDoctor='".$_GET["codigo"]."'";
			}
			if ($_GET["tipo"] == "paciente") {
				$filtro = "WHERE historial.codPaciente='".$_GET["codigo"]."'";
			}
		}
		$consulta = $DB_con->prepare("SELECT historial.id, pacientes.expediente, pacientes.nombres, pacientes.apellidos, doctor.nombre, historial.fecha, historial.hora_inicio, historial.hora_fin, historial.gravedad FROM historial INNER JOIN pacientes ON historial.codPaciente = pacientes.expediente INNER JOIN doctor ON  historial.codDoctor = doctor.codigo $filtro");
		$consulta->execute();
		$datos = $consulta->fetchAll(PDO::FETCH_ASSOC);
	?>
	<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Historial de Citas</h6>
            </div>
            <div class="card-body">
            	<nav class="navbar navbar-light bg-light">
  <form class="form-inline" action="Listar.php" method="GET">
  	<div class="input-group mb-3">
    <input class="form-control" type="search" name="codigo" placeholder="Codigo" aria-label="Search">
    <div class="input-group-append">
    <select name="tipo" class="custom-select" style="border-radius: 0; margin-right: 16px;" id="inputGroupSelect03" aria-label="Example select with button addon">
    <option value="paciente" selected>Paciente</option>
    <option value="doctor">Doctor</option>
  </select>
  </div>
  
  <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
</div>
  </form>
</nav>
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Paciente</th>
                      <th>Doctor</th>
                      <th>Fecha</th>
                      <th>Hora Inicio</th>
                      <th>Hora Fin</th>
                      <th>Urgencia</th>
                      <th>Accion</th>
                    </tr>
                  </thead>
                  <tbody>
                  	<?php
                  	foreach ($datos as $key => $value) {
                  	?>
                    <tr>
                      <td><?php echo $value["nombres"]." ".$value["apellidos"] ; ?></td>
                      <td><?php echo $value["nombre"]; ?></td>
                      <td><?php echo $value["fecha"]; ?></td>
                      <td><?php echo $value["hora_inicio"]; ?></td>
                      <td><?php echo $value["hora_fin"]; ?></td>
                      <td><?php echo $value["gravedad"]; ?></td>
                      <td><a href="Detalles.php?idconsulta=<?php echo $value["id"]; ?>">Detalles</a></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

</div>

<?php include '../footer.php'; ?>
