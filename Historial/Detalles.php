<!-- Topbar -->
<?php 
$idconsulta = $_GET['idconsulta'];
require '../Database/Conexion.class.php';
$consulta = $DB_con->prepare("SELECT * FROM historial WHERE id = '$idconsulta'");
$consulta->execute();
$datoscons = $consulta->fetch(PDO::FETCH_ASSOC);

$paciente = $datoscons["codPaciente"];
$consulta = $DB_con->prepare("SELECT * FROM pacientes WHERE expediente = '$paciente'");
$consulta->execute();
$datospac = $consulta->fetch(PDO::FETCH_ASSOC);

function calcularEdad($fecha_nac){
	$cumpleanos = new DateTime($fecha_nac);
    $hoy = new DateTime();
    $annos = $hoy->diff($cumpleanos);
    echo $annos->y;
}

include '../navbar.php'; ?>
<!-- End of Topbar -->

<!-- Begin Page Content -->
<div class="container-fluid">
<div class="card shadow mb-4">
            <div class="card-header py-3 clearfix">
              <h6 class="m-0 font-weight-bold text-primary float-left">Detalles de la consulta</h6>
              <a class="btn btn-warning float-right text-white" onclick="imprimir()">Imprimir</a>
            </div>
            <div class="card-body" id="cuerpo">
              

              	<!-- ---------------------------------------------------------------------------------->

              	<div class="card">
              		<div class="card-header">
              			<h6>Datos Generales</h6>
              		</div>
              		<div class="card-body">
                    <div class="row">
                    <div class="input-group col-md-3 input-group-sm mb-3">
              
    <span class="input-group-text bg-white border-0"><b>Doctor:</b>&nbsp;<?php echo strtoupper($datoscons['codDoctor']); ?></span>
  
              
            </div>
            <div class="input-group col-md-3 input-group-sm mb-3">
              
    <span class="input-group-text bg-white border-0"><b>Fecha:</b>&nbsp;<?php echo strtoupper($datoscons['fecha']); ?></span>
  
              
            </div>
            <div class="input-group col-md-3 input-group-sm mb-3">
              
    <span class="input-group-text bg-white border-0"><b>Hora Inicio:</b>&nbsp;<?php echo strtoupper($datoscons['hora_inicio']); ?></span>
  
              
            </div>
            <div class="input-group col-md-3 input-group-sm mb-3">
              
    <span class="input-group-text bg-white border-0"><b>Hora Fin:</b>&nbsp;<?php echo strtoupper($datoscons['hora_fin']); ?></span>
  
              
            </div>
                  </div>
              			<div class="input-group">
			         
               <span class="input-group-text bg-white border-0"><b>Expediente:</b>&nbsp;<?php echo strtoupper($datospac['expediente']); ?></span>
						    
                <span class="input-group-text bg-white border-0"><b>Gravedad:</b>&nbsp;<?php echo strtoupper($datoscons['gravedad']); ?></span>
						</div>
            <div class="input-group">
						 <span class="input-group-text bg-white border-0"><b>Nombre del paciente:</b>&nbsp;<?php echo strtoupper($datospac['nombres'].' '.$datospac['apellidos']); ?></span>
            			</div>
            			<div class="row">
            				<div class="input-group col-md-2 input-group-sm mb-3">
              
                  <span class="input-group-text bg-white border-0"><b>Edad:</b>&nbsp;<?php echo calcularEdad($datospac['fecha_nac']); ?> Años</span>
                </div>
              
            <div class="input-group col-md-3 input-group-sm mb-3">
              
            <span class="input-group-text bg-white border-0"><b>Genero:</b>&nbsp;<?php echo strtoupper($datospac['genero']); ?></span>
             </div>

            <div class="input-group col-md-3 input-group-sm mb-3">
              
    <span class="input-group-text bg-white border-0"><b>Padecimiento:</b>&nbsp;<?php echo strtoupper($datospac['padecimiento']); ?></span>
  
              
            </div>
            

            			</div>


                  
                  

              		</div>
              		
              	</div>

              	<!-- ---------------------------------------------------------------------------------->

              	<br><div class="card">
              		<div class="card-header">
              			<h6>Signos vitales</h6>
              		</div>
              		<div class="card-body">
              			<div class="row">
              				<div class="input-group col-md-3">
              <div class="input-group-prepend">
    <span class="input-group-text bg-white border-0">Temperatura: </span>
  </div>
              <input id="nombrepaciente" type="number" readonly name="Temperatura" class="form-control bg-light" value="<?php echo $datoscons['pacTemperatura']; ?>"><div class="input-group-append">
    <span class="input-group-text">° C</span>
  </div>
            </div>
            <div class="input-group col-md-3">
              <div class="input-group-prepend">
    <span class="input-group-text bg-white border-0">Peso: </span>
  </div>
              <input id="peso" type="number" name="Peso" readonly class="form-control bg-light" value="<?php echo $datoscons['pacPeso']; ?>">
              <div class="input-group-append">
    <span class="input-group-text">kg</span>
  </div>
            </div>
            <div class="input-group col-md-3">
              <div class="input-group-prepend">
    <span class="input-group-text bg-white border-0">Estatura: </span>
  </div>
              <input id="estatura" type="number" name="Estatura" readonly class="form-control bg-light" value="<?php echo $datoscons['pacEstatura']; ?>">
              <div class="input-group-append">
    <span class="input-group-text">cm</span>
  </div>
            </div>
            <div class="input-group col-md-3">
              <div class="input-group-prepend">
    <span class="input-group-text bg-white border-0">IMC: </span>
  </div>
              <input id="imc" type="number" name="IMC" step="any" readonly class="form-control bg-light" value="<?php echo $datoscons['pacIMC']; ?>">
              
            </div>
              			</div>
              		</div>
              	</div>

              	<!-- ---------------------------------------------------------------------------------->

              	<br><div class="card">
              		<div class="card-header">
              			<h6>Diagnostico</h6>
              		</div>
              		<div class="card-body">
              			<div class="input-group">
  <div class="input-group-prepend">
    <span class="input-group-text">Sintomas</span>
  </div>
  <textarea class="form-control" name="Sintomas" aria-label="With textarea" disabled><?php echo strtoupper($datoscons['diagnostico']); ?></textarea>
</div>

              		</div>
              	</div>
              	<br><div class="card">
              		<div class="card-header">
              			<h6>Tratamiento</h6>
              		</div>
              		<div class="card-body">
              			<div class="input-group">
  <div class="input-group-prepend">
    <span class="input-group-text">Tratamiento</span>
  </div>
  <textarea class="form-control" name="Tratamiento" aria-label="With textarea" disabled><?php echo strtoupper($datoscons['tratamiento']); ?></textarea>
</div>
              		</div>
              	</div>
              
            </div>
            <div class="card-footer">
            	<input type="date" hidden name="fecha" required id="fecha">
            	<input type="text" hidden name="horainicio" required id="horainicio">
            	<input type="text" hidden name="horafin" id="horafin">
            	
            	
            </div>
          </div>

</div>
<script type="text/javascript" src="https://cdn.rawgit.com/JDMcKinstry/JavaScriptDateFormat/master/Date.format.min.js"></script>
<script type="text/javascript">
	function imprimir(){
    var titulo = `<link href="/control-de-citas-medicas/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="/control-de-citas-medicas/css/sb-admin-2.min.css" rel="stylesheet"><center>
<h2>Control de Citas Medicas</h2>
<h5>DATOS DE CONSULTA DE PACIENTE</h5>
</center>`;
    var cuerpo = document.querySelector('#cuerpo').outerHTML;
    var ventana = window.open("", 'width=720,height=1000, left=300');
    var documento = ventana.document;
    documento.open();
    documento.write(titulo);
    documento.write(cuerpo);
    documento.close();
    ventana.onload = function(){
      ventana.print();
    }
    
  }
</script>

