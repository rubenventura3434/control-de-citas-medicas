<!-- Topbar -->
<?php 
$codPaciente = $_GET['codPaciente'];
$idconsulta = $_GET['idconsulta'];
$gravedad = $_GET['gravedad'];
require '../Database/Conexion.class.php';
$consulta = $DB_con->prepare("SELECT * FROM pacientes WHERE expediente = '$codPaciente'");
$consulta->execute();
$datospac = $consulta->fetch(PDO::FETCH_ASSOC);

function calcularEdad($fecha_nac){
	$cumpleanos = new DateTime($fecha_nac);
    $hoy = new DateTime();
    $annos = $hoy->diff($cumpleanos);
    echo $annos->y;
}

include '../navbar.php'; ?>
<!-- End of Topbar -->

<!-- Begin Page Content -->
<div class="container-fluid">
<div class="card shadow mb-4">
	<form action="Procesar.php" method="POST" onsubmit="return validarFormulario()">
            <div class="card-header py-3 clearfix">
              <h6 class="m-0 font-weight-bold text-primary float-left">Realizar Diagnostico</h6>
              <h6 class="m-0 font-weight-bold text-secondary float-right"><span id="fechatag">12/03/2019</span> <span id="horatag">12:29pm</span></h6>
            </div>
            <div class="card-body">
              

              	<!-- ---------------------------------------------------------------------------------->

              	<div class="card">
              		<div class="card-header">
              			<h6>Datos del paciente</h6>
              		</div>
              		<div class="card-body">
              			<div class="input-group">
			              <div class="input-group-prepend">
						    <span class="input-group-text bg-white border-0">Nombre del paciente: </span>
						  </div>
						  <input name="expediente" type="hidden" value="<?php echo $codPaciente; ?>">
						  <input name="doctor" type="hidden" value="<?php echo $datos['codigo']; ?>">
						  <input name="idconsulta" type="hidden" value="<?php echo $idconsulta; ?>">
						  <input name="gravedad" type="hidden" value="<?php echo $gravedad; ?>">
              				<input id="nombrepaciente" type="text" class="form-control bg-light" value="<?php echo $datospac['nombres'].' '.$datospac['apellidos']; ?>">
            			</div><br>
            			<div class="row">
            				<div class="input-group col-md-2 input-group-sm mb-3">
              <div class="input-group-prepend">
    <span class="input-group-text bg-white border-0">Edad: </span>
  </div>
              <input disabled id="nombrepaciente" type="text" class="form-control bg-light small" value="<?php echo calcularEdad($datospac['fecha_nac']); ?>"><div class="input-group-append">
    <span class="input-group-text">Años</span>
  </div>
            </div>
            <div class="input-group col-md-3 input-group-sm mb-3">
              <div class="input-group-prepend">
    <span class="input-group-text bg-white border-0">Genero: </span>
  </div>
              <input disabled id="nombrepaciente" type="text" class="form-control bg-light" value="<?php echo $datospac['genero']; ?>">
            </div>
            <div class="input-group col-md-3 input-group-sm mb-3">
              <div class="input-group-prepend">
    <span class="input-group-text bg-white border-0">Padecimiento: </span>
  </div>
              <input disabled id="nombrepaciente" type="text" class="form-control bg-light" value="<?php echo $datospac['padecimiento']; ?>">
            </div>
            			</div>
              		</div>
              		<div class="card-footer">
              			<a class="btn btn-warning float-right text-white" onclick="window.open('http://localhost/control-de-citas-medicas/Historial/Listar.php?tipo=paciente&codigo=<?php echo $codPaciente; ?>','Historial', 'width=720,height=1000, left=300')">Historial de consultas</a>
              		</div>
              	</div>

              	<!-- ---------------------------------------------------------------------------------->

              	<br><div class="card">
              		<div class="card-header">
              			<h6>Signos vitales</h6>
              		</div>
              		<div class="card-body">
              			<div class="row">
              				<div class="input-group col-md-3">
              <div class="input-group-prepend">
    <span class="input-group-text bg-white border-0">Temperatura: </span>
  </div>
              <input id="nombrepaciente" type="number" name="Temperatura" class="form-control bg-light" value="37"><div class="input-group-append">
    <span class="input-group-text">° C</span>
  </div>
            </div>
            <div class="input-group col-md-2">
              <div class="input-group-prepend">
    <span class="input-group-text bg-white border-0">Peso: </span>
  </div>
              <input id="peso" type="number" name="Peso" class="form-control bg-light" value="80" onchange="calcularIMC()">
              <div class="input-group-append">
    <span class="input-group-text">kg</span>
  </div>
            </div>
            <div class="input-group col-md-3">
              <div class="input-group-prepend">
    <span class="input-group-text bg-white border-0">Estatura: </span>
  </div>
              <input id="estatura" type="number" name="Estatura" class="form-control bg-light" value="160" onchange="calcularIMC()">
              <div class="input-group-append">
    <span class="input-group-text">cm</span>
  </div>
            </div>
            <div class="input-group col-md-4">
              <div class="input-group-prepend">
    <span class="input-group-text bg-white border-0">IMC: </span>
  </div>
              <input id="imc" type="number" name="IMC" step="any" readonly class="form-control bg-light" value="17">
              <div class="input-group-append">
    <span id="imctag" class="input-group-text bg-success text-light">Normal</span>
  </div>
            </div>
              			</div>
              		</div>
              	</div>

              	<!-- ---------------------------------------------------------------------------------->

              	<br><div class="card">
              		<div class="card-header">
              			<h6>Diagnostico</h6>
              		</div>
              		<div class="card-body">
              			<div class="input-group">
  <div class="input-group-prepend">
    <span class="input-group-text">Sintomas</span>
  </div>
  <textarea class="form-control" name="Sintomas" aria-label="With textarea"></textarea>
</div>

              		</div>
              	</div>
              	<br><div class="card">
              		<div class="card-header">
              			<h6>Tratamiento</h6>
              		</div>
              		<div class="card-body">
              			<div class="input-group">
  <div class="input-group-prepend">
    <span class="input-group-text">Tratamiento</span>
  </div>
  <textarea class="form-control" name="Tratamiento" aria-label="With textarea" required></textarea>
</div>
              		</div>
              	</div>
              
            </div>
            <div class="card-footer">
            	<input type="date" hidden name="fecha" required id="fecha">
            	<input type="text" hidden name="horainicio" required id="horainicio">
            	<input type="text" hidden name="horafin" id="horafin">
            	<div class="float-right">
            		<button name="guardarImprimirConsulta" class="btn btn-primary">Guardar e Imprimir</button>
            		<input type="submit" name="guardarConsulta" value="Guardar" class="btn btn-success">
            	</div>
            	
            </div></form>
          </div>

</div>
<script type="text/javascript" src="https://cdn.rawgit.com/JDMcKinstry/JavaScriptDateFormat/master/Date.format.min.js"></script>
<script type="text/javascript">
	function calcularIMC(){
		var peso = document.querySelector('#peso').value;
      	var altura = document.querySelector('#estatura').value;
      	altura = altura / 100;
      	var imc = peso / (altura * altura);
      	var txtIcm = document.querySelector('#imc');
      	var tagIcm = document.querySelector('#imctag');
      	txtIcm.value = Math.round(imc*100)/100;
      	var estado = "";
      	if(imc<16){
          estado="Delgadez Severa";    
        }
        else if(imc<17){
          estado="Delgadez Moderada";    
        }
        else if(imc<18.5){
          estado="Delgadez Aceptable";   
        }
        else if(imc<25){
          estado="Peso Normal";    
        }
        else if(imc<30){
          estado="Sobrepeso";    
        }
        else if(imc<35){
          estado="Obeso: Tipo I";    
        }
        else if(imc<40){
          estado="Obeso: Tipo II";   
        }
        else if(imc>=40){
          estado="Obeso: Tipo III";    
        }
        tagIcm.textContent = estado;

	}
	document.addEventListener("DOMContentLoaded", function() {
   		var date = new Date().format("d/m/Y");
   		var fecha = new Date().format("Y-m-d");
   		var time = new Date().format("h:ia");
   		var hora = new Date().format("H:i:s");
   		document.querySelector("#fechatag").textContent = date;
   		document.querySelector("#fecha").value = fecha;
   		document.querySelector("#horatag").textContent = time;
   		document.querySelector("#horainicio").value = hora;
	});
	function validarFormulario(){
		var hora = new Date().format("H:i:s");
		document.querySelector("#horafin").value = hora;
		return true;
	};
</script>


<?php include '../footer.php'; ?>
