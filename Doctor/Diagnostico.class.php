<?php
require '../Database/Conexion.class.php';

class Diagnostico
{
	private $idconsulta;
	private $doctor;
	private $expediente;
	private $temperatura;
	private $peso;
	private $altura;
	private $imc;
	private $sintomas;
	private $tratamiento;
	private $horaincio;
	private $horafin;
	private $gravedad;
	private $fecha;
	private $dbcon;

	function setDiagnostico($idconsulta,$doctor, $expediente, $fecha, $horaincio, $horafin, $gravedad, $temperatura, $peso, $altura, $imc, $sintomas,	$tratamiento, $dbcon){

	$this->idconsulta = $idconsulta;
	$this->doctor = $doctor;
	$this->expediente = $expediente;
	$this->temperatura = $temperatura;
	$this->peso = $peso;
	$this->altura = $altura;
	$this->imc = $imc;
	$this->sintomas = $sintomas;
	$this->tratamiento = $tratamiento;
	$this->horaincio = $horaincio;
	$this->horafin = $horafin;
	$this->fecha = $fecha;
	$this->gravedad = $gravedad;
	$this->dbcon = $dbcon;
	}
	function guardarConsulta(){
		try {
		$consulta = $this->dbcon->prepare("INSERT INTO historial VALUES (null, '$this->doctor', '$this->expediente', '$this->fecha', '$this->horaincio', '$this->horafin', '$this->gravedad', '$this->temperatura', '$this->peso', '$this->altura', '$this->imc', '$this->sintomas',	'$this->tratamiento')");
		if ($consulta->execute()) {
			echo "Consulta Registrada.";
			$this->eliminarCita();
		} else {
			echo "Error al registrar la consulta!";
		}
		} catch(PDOEXCEPTION $e){
			$e->getMessage();
		}
	}
	function eliminarCita(){
		try {
		echo "Eliminando cita.";
		$consulta = $this->dbcon->prepare("DELETE FROM citas WHERE id = '$this->idconsulta'");
		if ($consulta->execute()) {
			echo "Cita eliminada.";
			header("Location: Citas.php");
		} else {
			echo "Error al eliminar la cita!";
		}
		} catch(PDOEXCEPTION $e){
			$e->getMessage();
		}
	}

}

?>