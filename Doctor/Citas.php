<!-- Topbar -->
<?php include '../navbar.php'; ?>
<!-- End of Topbar -->

<!-- Begin Page Content -->
<div class="container-fluid">

	<?php
		require '../Database/Conexion.class.php';
		$doctor = $datos["codigo"];
		$consulta = $DB_con->prepare("SELECT citas.id, pacientes.expediente, pacientes.nombres, pacientes.apellidos, citas.fecha, citas.hora, citas.gravedad FROM citas INNER JOIN pacientes ON citas.codPaciente = pacientes.expediente WHERE citas.codDoctor='$doctor'");
		$consulta->execute();
		$datos = $consulta->fetchAll(PDO::FETCH_ASSOC);
	?>
	<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Mis Citas</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Paciente</th>
                      <th>Fecha</th>
                      <th>Hora</th>
                      <th>Urgencia</th>
                      <th>Acción</th>
                    </tr>
                  </thead>
                  <tbody>
                  	<?php
                  	foreach ($datos as $key => $value) {
                  	?>
                    <tr>
                      <td><?php echo $value["nombres"]." ".$value["apellidos"] ; ?></td>
                      <td><?php echo $value["fecha"]; ?></td>
                      <td><?php echo $value["hora"]; ?></td>
                      <td><?php echo $value["gravedad"]; ?></td>
                      <td><a href="Consulta.php?codPaciente=<?php echo $value['expediente']; ?>&idconsulta=<?php echo $value['id']; ?>&gravedad=<?php echo $value['gravedad']; ?>" class="btn btn-success">Realizar</a></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

</div>

<?php include '../footer.php'; ?>
